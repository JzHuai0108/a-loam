#ifndef __POINT_CLOUD_CONVERSION_HPP__
#define __POINT_CLOUD_CONVERSION_HPP__
#include <pcl/common/common.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <sensor_msgs/PointCloud2.h>
#include "common.h"
#include "pcl_utils.h"

template <typename PointT>
void removeClosedPointCloud(const pcl::PointCloud<PointT> &cloud_in,
                            pcl::PointCloud<PointT> &cloud_out, float thres) {
    if (&cloud_in != &cloud_out) {
        cloud_out.header = cloud_in.header;
        cloud_out.points.resize(cloud_in.points.size());
    }

    size_t j = 0;

    for (size_t i = 0; i < cloud_in.points.size(); ++i) {
        if (cloud_in.points[i].x * cloud_in.points[i].x +
                cloud_in.points[i].y * cloud_in.points[i].y +
                cloud_in.points[i].z * cloud_in.points[i].z < thres * thres)
            continue;
        cloud_out.points[j] = cloud_in.points[i];
        j++;
    }
    if (j != cloud_in.points.size()) {
        cloud_out.points.resize(j);
    }

    cloud_out.height = 1;
    cloud_out.width = static_cast<uint32_t>(j);
    cloud_out.is_dense = true;
}

namespace jaguar {
void PointCloud2Callback(const sensor_msgs::PointCloud2& cloud_msg, 
    pcl::PointCloud<TPoint>* cloud, std::string="");

void printPointCloud(const pcl::PointCloud<pcl::PointXYZI>& laserCloudIn);

class FeatureExtractor {
private:
    float* cloudCurvature;
    int* cloudSortInd;
    int* cloudNeighborPicked;
    int* cloudLabel;

    Eigen::Quaterniond q_lb;

    int ds_rate = 2;
    double ds_v = 0.6;

    const int halfwindow = 5;

public:
FeatureExtractor(float* _cloudCurvature,
    int* _cloudSortInd,
    int* _cloudNeighborPicked,
    int* _cloudLabel): cloudCurvature(_cloudCurvature), cloudSortInd(_cloudSortInd),
       cloudNeighborPicked(_cloudNeighborPicked), cloudLabel(_cloudLabel) {
}

void setq_lb(const Eigen::Quaterniond& _q_lb) {
   q_lb = _q_lb;
}

void setds_rate(int rate) {
  ds_rate = rate;
}

void setLeafSize(double leafSize) {
  ds_v = leafSize;
}

~FeatureExtractor() {}

VPoint undistortion(VPoint pt, const Eigen::Quaterniond& qIMU) {
    double dt = 0.05;
    int line = int(pt.intensity);
    double dt_i = pt.intensity - line;

    double ratio_i = dt_i / dt;
    if(ratio_i >= 1.0) {
        ratio_i = 1.0;
    }

    Eigen::Quaterniond q0 = Eigen::Quaterniond::Identity();
    Eigen::Quaterniond q_si = q0.slerp(ratio_i, qIMU);

    Eigen::Vector3d pt_i(pt.x, pt.y, pt.z);

    q_si = q_lb * q_si * q_lb.inverse();
    Eigen::Vector3d pt_s = q_si * pt_i;

    VPoint p_out;
    p_out.x = pt_s.x();
    p_out.y = pt_s.y();
    p_out.z = pt_s.z();
    p_out.intensity = pt.intensity;
    return p_out;
}

int splitScan(pcl::PointCloud<TPoint> &lidar_cloud_in, const Eigen::Quaterniond& qIMU, 
    std::vector<pcl::PointCloud<VPoint>>& laserCloudScans);

template <typename PointType>
typename pcl::PointCloud<PointType>::Ptr concatenateSubscans(
    const std::vector<pcl::PointCloud<PointType>>& laserCloudScans,
    std::vector<int>& scanStartInd,
    std::vector<int>& scanEndInd) {
    typename pcl::PointCloud<PointType>::Ptr laserCloud(new pcl::PointCloud<PointType>());
    size_t numSubscans = laserCloudScans.size();
    scanStartInd.resize(numSubscans);
    scanEndInd.resize(numSubscans);
    for (size_t i = 0u; i < numSubscans; i++) {
        scanStartInd[i] = laserCloud->size() + 5;
        *laserCloud += laserCloudScans[i];
        scanEndInd[i] = laserCloud->size() - 6;
    }
    return laserCloud;
}

template <typename PointType>
void computeCurvature(typename pcl::PointCloud<PointType>::Ptr laserCloud, int cloudSize) {
    for (int i = halfwindow; i < cloudSize - halfwindow; i++) {
        float diffX = laserCloud->points[i - 5].x + laserCloud->points[i - 4].x + laserCloud->points[i - 3].x + 
          laserCloud->points[i - 2].x + laserCloud->points[i - 1].x - 10 * laserCloud->points[i].x +
          laserCloud->points[i + 1].x + laserCloud->points[i + 2].x + laserCloud->points[i + 3].x + 
          laserCloud->points[i + 4].x + laserCloud->points[i + 5].x;
        float diffY = laserCloud->points[i - 5].y + laserCloud->points[i - 4].y + laserCloud->points[i - 3].y + 
          laserCloud->points[i - 2].y + laserCloud->points[i - 1].y - 10 * laserCloud->points[i].y + 
          laserCloud->points[i + 1].y + laserCloud->points[i + 2].y + laserCloud->points[i + 3].y + 
          laserCloud->points[i + 4].y + laserCloud->points[i + 5].y;
        float diffZ = laserCloud->points[i - 5].z + laserCloud->points[i - 4].z + laserCloud->points[i - 3].z + 
          laserCloud->points[i - 2].z + laserCloud->points[i - 1].z - 10 * laserCloud->points[i].z + 
          laserCloud->points[i + 1].z + laserCloud->points[i + 2].z + laserCloud->points[i + 3].z + 
          laserCloud->points[i + 4].z + laserCloud->points[i + 5].z;

        cloudCurvature[i] = diffX * diffX + diffY * diffY + diffZ * diffZ;
        cloudSortInd[i] = i;
        cloudNeighborPicked[i] = 0;
        cloudLabel[i] = 0;
    }
}

bool comp (int i,int j) { return (cloudCurvature[i]<cloudCurvature[j]);}

template <typename PointType>
void extractFeatures(typename pcl::PointCloud<PointType>::ConstPtr laserCloud,
    const std::vector<int>& scanStartInd,
    const std::vector<int>& scanEndInd,
    pcl::PointCloud<PointType>& cornerPointsSharp,
    pcl::PointCloud<PointType>& cornerPointsLessSharp,
    pcl::PointCloud<PointType>& surfPointsFlat,
    pcl::PointCloud<PointType>& surfPointsLessFlat) {
    size_t numSubscans = scanStartInd.size();
    for (size_t i = 0; i < numSubscans; i++) {
        if( scanEndInd[i] - scanStartInd[i] < 6 || i % ds_rate != 0)
            continue;
        typename pcl::PointCloud<PointType>::Ptr surfPointsLessFlatScan(new pcl::PointCloud<PointType>);
        int sp = scanStartInd[i];
        int ep = scanEndInd[i];

        auto bound_comp = bind(&FeatureExtractor::comp, this, _1, _2);
        std::sort(cloudSortInd + sp, cloudSortInd + ep + 1, bound_comp);

        int largestPickedNum = 0;
        for (int k = ep; k >= sp; k--) {
            int ind = cloudSortInd[k];
            if (cloudNeighborPicked[ind] == 0 &&
                    cloudCurvature[ind] > 2.0) {
                largestPickedNum++;
                if (largestPickedNum <= 2) {
                    cloudLabel[ind] = 2;
                    cornerPointsSharp.push_back(laserCloud->points[ind]);
                    cornerPointsLessSharp.push_back(laserCloud->points[ind]);
                }
                else if (largestPickedNum <= 10) {
                    cloudLabel[ind] = 1;
                    cornerPointsLessSharp.push_back(laserCloud->points[ind]);
                }
                else
                    break;

                cloudNeighborPicked[ind] = 1;

                for (int l = 1; l <= 5; l++) {
                    float diffX = laserCloud->points[ind + l].x - laserCloud->points[ind + l - 1].x;
                    float diffY = laserCloud->points[ind + l].y - laserCloud->points[ind + l - 1].y;
                    float diffZ = laserCloud->points[ind + l].z - laserCloud->points[ind + l - 1].z;
                    if (diffX * diffX + diffY * diffY + diffZ * diffZ > 0.05)
                        break;

                    cloudNeighborPicked[ind + l] = 1;
                }
                for (int l = -1; l >= -5; l--) {
                    float diffX = laserCloud->points[ind + l].x - laserCloud->points[ind + l + 1].x;
                    float diffY = laserCloud->points[ind + l].y - laserCloud->points[ind + l + 1].y;
                    float diffZ = laserCloud->points[ind + l].z - laserCloud->points[ind + l + 1].z;
                    if (diffX * diffX + diffY * diffY + diffZ * diffZ > 0.05)
                        break;

                    cloudNeighborPicked[ind + l] = 1;
                }
            }
        }

        int smallestPickedNum = 0;
        for (int k = sp; k <= ep; k++) {
            int ind = cloudSortInd[k];

            if(laserCloud->points[ind].x*laserCloud->points[ind].x+laserCloud->points[ind].y*laserCloud->points[ind].y+
                laserCloud->points[ind].z*laserCloud->points[ind].z < 0.25)
                continue;

            if (cloudNeighborPicked[ind] == 0 &&
                    cloudCurvature[ind] < 0.1) {
                cloudLabel[ind] = -1;
                surfPointsFlat.push_back(laserCloud->points[ind]);

                smallestPickedNum++;
                if (smallestPickedNum >= 4)
                    break;

                cloudNeighborPicked[ind] = 1;
                for (int l = 1; l <= 5; l++) {
                    float diffX = laserCloud->points[ind + l].x - laserCloud->points[ind + l - 1].x;
                    float diffY = laserCloud->points[ind + l].y - laserCloud->points[ind + l - 1].y;
                    float diffZ = laserCloud->points[ind + l].z - laserCloud->points[ind + l - 1].z;
                    if (diffX * diffX + diffY * diffY + diffZ * diffZ > 0.05)
                        break;

                    cloudNeighborPicked[ind + l] = 1;
                }
                for (int l = -1; l >= -5; l--) {
                    float diffX = laserCloud->points[ind + l].x - laserCloud->points[ind + l + 1].x;
                    float diffY = laserCloud->points[ind + l].y - laserCloud->points[ind + l + 1].y;
                    float diffZ = laserCloud->points[ind + l].z - laserCloud->points[ind + l + 1].z;
                    if (diffX * diffX + diffY * diffY + diffZ * diffZ > 0.05)
                        break;

                    cloudNeighborPicked[ind + l] = 1;
                }
            }
        }

        for (int k = sp; k <= ep; k++) {
            if(laserCloud->points[k].x*laserCloud->points[k].x+laserCloud->points[k].y*laserCloud->points[k].y+
                laserCloud->points[k].z*laserCloud->points[k].z < 0.25)
                continue;
            if (cloudLabel[k] <= 0)
                surfPointsLessFlatScan->push_back(laserCloud->points[k]);
        }

        pcl::PointCloud<PointType> surfPointsLessFlatScanDS;
        pcl::VoxelGrid<PointType> downSizeFilter;
        downSizeFilter.setInputCloud(surfPointsLessFlatScan);
        downSizeFilter.setLeafSize(ds_v, ds_v, ds_v);
        downSizeFilter.filter(surfPointsLessFlatScanDS);

        surfPointsLessFlat += surfPointsLessFlatScanDS;
    }
}
};

}  // namespace jaguar

#endif  // __POINT_CLOUD_CONVERSION_HPP__