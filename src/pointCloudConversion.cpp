#include "aloam_velodyne/pointCloudConversion.hpp"
#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/filters/filter.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>

#include "aloam_velodyne/pcl_utils.h"

namespace jaguar {

void printPointCloud(const pcl::PointCloud<pcl::PointXYZI>& laserCloudIn) {
  unsigned int pts_size = laserCloudIn.size();
  for ( unsigned int idx = 0; idx < pts_size; idx++ )
  {
    const pcl::PointXYZI& p = laserCloudIn.points[ idx ];
    std::cout << p.x << ", " << p.y << ", " << p.z << ", " << p.intensity << "\n";
  }
  std::cout << std::endl;
}

void PointCloud2Callback(const sensor_msgs::PointCloud2& cloud_msg, pcl::PointCloud<TPoint>* cloud, std::string outputFile)
{
  int pointBytes = cloud_msg.point_step;
  int offset_x = 0;
  int offset_y = 0;
  int offset_z = 0;
  int offset_time = 0;
  int offset_int = 0;
  for (size_t f=0; f<cloud_msg.fields.size(); ++f)
  {
    if (cloud_msg.fields[f].name == "x")
      offset_x = cloud_msg.fields[f].offset;
    if (cloud_msg.fields[f].name == "y")
      offset_y = cloud_msg.fields[f].offset;
    if (cloud_msg.fields[f].name == "z")
      offset_z = cloud_msg.fields[f].offset;
    if (cloud_msg.fields[f].name == "timestamp")
      offset_time = cloud_msg.fields[f].offset;
    if (cloud_msg.fields[f].name == "intensity")
      offset_int = cloud_msg.fields[f].offset;
  }

  // populate point cloud object
  cloud->clear();
  cloud->header = pcl_conversions::toPCL(cloud_msg.header);
  cloud->height = cloud_msg.height;
  cloud->width = cloud_msg.width;
  cloud->is_dense = false;
  cloud->resize(cloud_msg.height * cloud_msg.width);

  for (size_t p=0; p<cloud_msg.width; ++p)
  {
      TPoint newPoint;
      float x = *(float*)(&cloud_msg.data[0] + (pointBytes*p) + offset_x);
      float y = *(float*)(&cloud_msg.data[0] + (pointBytes*p) + offset_y);
      float z = *(float*)(&cloud_msg.data[0] + (pointBytes*p) + offset_z);
      // up-right-forward
      newPoint.x = x;
      newPoint.y = y;
      newPoint.z = z;
      newPoint.timestamp = *(double*)(&cloud_msg.data[0] + (pointBytes*p) + offset_time);
      newPoint.intensity = *(unsigned char*)(&cloud_msg.data[0] + (pointBytes*p) + offset_int);
      cloud->at(p) = newPoint;
  }

  if (!outputFile.empty()) {
      std::ofstream ofs(outputFile, std::ofstream::out);
      for (size_t p=0; p<cloud_msg.width; ++p) {
        TPoint newPoint;
        newPoint.x = *(float*)(&cloud_msg.data[0] + (pointBytes*p) + offset_x);
        newPoint.y = *(float*)(&cloud_msg.data[0] + (pointBytes*p) + offset_y);
        newPoint.z = *(float*)(&cloud_msg.data[0] + (pointBytes*p) + offset_z);
        newPoint.timestamp = *(double*)(&cloud_msg.data[0] + (pointBytes*p) + offset_time);
        newPoint.intensity = *(unsigned char*)(&cloud_msg.data[0] + (pointBytes*p) + offset_int);
        ofs << std::setprecision(8) << newPoint.x << ", " << newPoint.y << ", " << newPoint.z << ", "
            << std::setprecision(20) << newPoint.timestamp << ", " << newPoint.intensity << "\n";
      }
      ofs << std::endl;
      ofs.close();
  }
}

// https://math.stackexchange.com/questions/1098487/atan2-faster-approximation
double approximateAtan2(double y, double x) {
  double absx = std::fabs(x);
  double absy = std::fabs(y);
  double a = std::min(absx, absy) / std::max(absx, absy);
  double s = a * a;
  double r = ((-0.0464964749 * s + 0.15931422) * s - 0.327622764) * s * a + a;
  if (absy > absx)
    r = 1.57079637 - r;
  if (x < 0) 
    r = 3.14159274 - r;
  if (y < 0)
    r = -r;
  return r;
}

struct LineSegment {
double time_;
size_t startindex_;
size_t endindex_;
double startelevation_;
double endelevation_;
size_t segmentid_;

LineSegment(double time,
            size_t startindex,
            size_t endindex,
            double startelevation,
            double endelevation,
            size_t segmentid = 0u) : 
    time_(time),
    startindex_(startindex),
    endindex_(endindex),
    startelevation_(startelevation),
    endelevation_(endelevation),
    segmentid_(segmentid) {}
};

// Note this function assumes the original frame used by Jaguar.
void sweepToSubscans(const pcl::PointCloud<TPoint> &data, std::vector<LineSegment>& segmentlist) {
  segmentlist.clear();
  segmentlist.reserve(2500);

  size_t segmentstartid = 0u;
  for (size_t i = 0u; i < data.size(); ++i) {
      if (data.points[i].timestamp == data.points[segmentstartid].timestamp)
          continue;
      double startelevation = atan2(data.points[segmentstartid].x, data.points[segmentstartid].z) * 57.3;
      double endelevation = atan2(data.points[i-1].x, data.points[i-1].z) * 57.3;
      segmentlist.emplace_back(data.points[segmentstartid].timestamp, segmentstartid, i-1, 
          startelevation, endelevation, 0);
      segmentstartid = i;
  }
  // add the last segment
  double startelevation = atan2(data.points[segmentstartid].x, data.points[segmentstartid].z) * 57.3;
  size_t lastindex = data.size() - 1;
  double endelevation = atan2(data.points[lastindex].x, data.points[lastindex].z) * 57.3;
  segmentlist.emplace_back(data.points[segmentstartid].timestamp, segmentstartid, lastindex, 
      startelevation, endelevation, 0);
 
  size_t midway = 0u;
  for (size_t i = 1; i < segmentlist.size(); ++i) {
    double dt = segmentlist[i].time_ - segmentlist[i-1].time_;
    if (dt < -0.04) { // this is half of the sample interval of Jaguar Prime.
      if (std::fabs(segmentlist[i].time_ - segmentlist[0].time_) < 0.02) {
        midway = i - 1;
        break;
      }
    }
  }
  if (midway == 0u) {
    ROS_WARN("Failed to separate hemisphere!");
  }
  // for a sweep of a jaguar prime lidar, split its points into two hemispheres
  std::vector<int> hemisphereId(data.size(), -1);
  for (size_t i = 0u; i <= segmentlist[midway].endindex_; ++i) {
    hemisphereId[i] = 1;
  }
  
  segmentlist[0].segmentid_ = 0u;
  for (size_t i = 1u; i <= midway; ++i) {
      double delta = segmentlist[i-1].endelevation_ - segmentlist[i].startelevation_;
      if (delta > 0)
          segmentlist[i].segmentid_ = segmentlist[i-1].segmentid_;
      else
          segmentlist[i].segmentid_ = segmentlist[i-1].segmentid_ + 1;
  }

  segmentlist[midway + 1].segmentid_ = segmentlist[midway].segmentid_ + 1;
  for (size_t i = midway + 2; i < segmentlist.size(); ++i) {
      double delta = segmentlist[i-1].endelevation_ - segmentlist[i].startelevation_;
      if (delta > 0)
          segmentlist[i].segmentid_ = segmentlist[i-1].segmentid_;
      else
          segmentlist[i].segmentid_ = segmentlist[i-1].segmentid_ + 1;
  }
  printf("Fractured segments %zu midway %zu spliced segments %zu\n", segmentlist.size(), midway, segmentlist.back().segmentid_ + 1);
}

int FeatureExtractor::splitScan(pcl::PointCloud<TPoint> &lidar_cloud_in, const Eigen::Quaterniond& qIMU, 
    std::vector<pcl::PointCloud<VPoint>>& laserCloudScans) {
    std::vector<int> indices;
    removeClosedPointCloud<TPoint>(lidar_cloud_in, lidar_cloud_in, 3.0);

    size_t cloudSize = lidar_cloud_in.points.size();
    double startTime = lidar_cloud_in.points[0].timestamp;
    // undistort with IMU data
    for (size_t i = 0u; i < cloudSize; i++) {
        VPoint point;
        point.x = lidar_cloud_in.points[i].x;
        point.y = lidar_cloud_in.points[i].y;
        point.z = lidar_cloud_in.points[i].z;
        int fakeIntensity = std::floor(lidar_cloud_in.points[i].intensity * 0.1);
        double relTime= lidar_cloud_in.points[i].timestamp - startTime;
        point.intensity = fakeIntensity + relTime;
        VPoint point_undis = undistortion(point, qIMU);
        lidar_cloud_in.points[i].x = point_undis.x;
        lidar_cloud_in.points[i].y = point_undis.y;
        lidar_cloud_in.points[i].z = point_undis.z;
    }

    // split into subscans
    std::vector<LineSegment> segmentlist;
    sweepToSubscans(lidar_cloud_in, segmentlist);

    laserCloudScans.clear();
    laserCloudScans.resize(segmentlist.back().segmentid_ + 1);
    for (size_t i = 0u; i < laserCloudScans.size(); ++i) {
        laserCloudScans[i].reserve(150);
    }
    for (size_t i = 0u; i < segmentlist.size(); ++i) {
      size_t segmentid = segmentlist[i].segmentid_;
      for (size_t j = segmentlist[i].startindex_; j <= segmentlist[i].endindex_; ++j) {
          VPoint point;
          point.x = lidar_cloud_in.points[j].x;
          point.y = lidar_cloud_in.points[j].y;
          point.z = lidar_cloud_in.points[j].z;
          point.intensity = lidar_cloud_in.points[j].intensity;
          laserCloudScans[segmentid].push_back(point);
      }
    }
    
    cloudSize = lidar_cloud_in.size();
    // printf("points size %zu sub-scans %zu\n", cloudSize, laserCloudScans.size());
    return (int)cloudSize;
}
}  // namespace jaguar