
aloam_ws=/media/jhuai/docker/lidarslam/aloam_ws
datadir=/media/jhuai/SeagateData/jhuai/data/homebrew
outdir=/media/jhuai/SeagateData/jhuai/results/aloam

cd $aloam_ws
source devel/setup.bash

roverbags=(
$datadir/mycar_nav/20220929/lidar4_aligned.bag
$datadir/mycar_nav/20221019/lidar3_aligned.bag
$datadir/mycar_nav/20221019/lidar4_aligned.bag
$datadir/mycar_nav/20221021/lidar3_aligned.bag
$datadir/mycar_nav/20221021/lidar4_aligned.bag
$datadir/mycar_nav/20230814/lidar6_aligned.bag
$datadir/mycar_nav/20230814/lidar7_aligned.bag
)

handheldbags=(
$datadir/handheld/20230921/data2_aligned.bag
$datadir/handheld/20230920/data2_aligned.bag
$datadir/handheld/20230921/data3_aligned.bag
$datadir/handheld/20230921/data4_aligned.bag
$datadir/handheld/20230921/data5_aligned.bag
$datadir/handheld/20231007/data5_aligned.bag
$datadir/handheld/20231019/data1_aligned.bag
$datadir/handheld/20231019/data2_aligned.bag
$datadir/handheld/20231025/data1_aligned.bag
)

proc_rover() {
    bag=$1
    echo "Processing $bag"
    dirname=$(dirname $bag)
    date=$(basename $dirname)
    bagname=$(basename $bag)
    bagname=${bagname%.*}
    echo "Bagname: $bagname"
    mkdir -p $outdir/$date
    roslaunch aloam_velodyne aloam_velodyne_VLP_16.launch bag:=$bag path_est:=$outdir/$date/$bagname.txt
}

proc_handheld() {
    bag=$1
    echo "Processing $bag"
    dirname=$(dirname $bag)
    date=$(basename $dirname)
    bagname=$(basename $bag)
    bagname=${bagname%.*}
    echo "Bagname: $bagname"
    mkdir -p $outdir/$date
    roslaunch aloam_velodyne aloam_hesai.launch bag:=$bag path_est:=$outdir/$date/$bagname.txt
}


for bag in ${roverbags[@]}; do
    proc_rover $bag
done

for bag in ${handheldbags[@]}; do
    proc_handheld $bag
done

