
aloam_ws=/media/jhuai/docker/lidarslam/aloam_ws
datadir=/media/jhuai/BackupPlus/jhuai/data/homebrew/handheld
outdir=/media/jhuai/BackupPlus/jhuai/results/homebrew_aloam

cd $aloam_ws
source devel/setup.bash

handheldbags=(
$datadir/20231105/data1_aligned.bag
$datadir/20231105/data2_aligned.bag
$datadir/20231105/data3_aligned.bag
$datadir/20231105/data4_aligned.bag
$datadir/20231105/data5_aligned.bag
$datadir/20231105/data6_aligned.bag
$datadir/20231105/data7_aligned.bag
$datadir/20231105_aft/data1_aligned.bag
$datadir/20231105_aft/data2_aligned.bag
$datadir/20231105_aft/data3_aligned.bag
$datadir/20231105_aft/data4_aligned.bag
$datadir/20231105_aft/data5_aligned.bag
$datadir/20231105_aft/data6_aligned.bag
$datadir/20231109/data1_aligned.bag
$datadir/20231109/data2_aligned.bag
$datadir/20231109/data3_aligned.bag
$datadir/20231109/data4_aligned.bag
)

proc_handheld() {
    bag=$1
    echo "Processing $bag"
    dirname=$(dirname $bag)
    date=$(basename $dirname)
    bagname=$(basename $bag)
    bagname=${bagname%.*}
    echo "Bagname: $bagname"
    mkdir -p $outdir/$date/$bagname
    roslaunch aloam_velodyne aloam_hesai.launch bag:=$bag path_est:=$outdir/$date/$bagname/state_estimates.txt
}

counter=0
for bag in ${handheldbags[@]}; do
    proc_handheld $bag
    ((counter++))
    # if [ $counter -ge 0 ]; then
    #     break
    # fi
done

