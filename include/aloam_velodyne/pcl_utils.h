/*
 * LI_Calib: An Open Platform for LiDAR-IMU Calibration
 * Copyright (C) 2020 Jiajun Lv
 * Copyright (C) 2020 Kewei Hu
 * Copyright (C) 2020 Jinhong Xu
 * Copyright (C) 2020 LI_Calib Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PCL_UTILS_H
#define PCL_UTILS_H

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>
#include <pcl/common/common.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <Eigen/Eigen>

namespace pcl {
struct PointXYZITR {
  PCL_ADD_POINT4D
  float intensity;
  double timestamp;
  uint32_t ring;
};
}

POINT_CLOUD_REGISTER_POINT_STRUCT(PointXYZITR,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (float, intensity, intensity)
                                  (double, timestamp, timestamp)
                                  (uint32_t, ring, ring))

namespace jaguar {

typedef pcl::PointXYZI VPoint;
typedef pcl::PointCloud<VPoint> VPointCloud;

typedef pcl::PointXYZRGB colorPointT;
typedef pcl::PointCloud<colorPointT> colorPointCloudT;

struct PointXYZIT {
  PCL_ADD_POINT4D
  float intensity;
  double timestamp;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW // make sure our new allocators are aligned
} EIGEN_ALIGN16;

inline void downsampleCloud(pcl::PointCloud<pcl::PointXYZI>::Ptr in_cloud,
                            pcl::PointCloud<pcl::PointXYZI>::Ptr out_cloud,
                            float in_leaf_size) {
  pcl::VoxelGrid<pcl::PointXYZI> sor;
  sor.setInputCloud(in_cloud);
  sor.setLeafSize((float)in_leaf_size, (float)in_leaf_size, (float)in_leaf_size);
  sor.filter(*out_cloud);
}

template <typename T>
inline T r2(T x, T y, T z) {
  return x * x + y * y + z * z;
}

inline float r2(const VPoint& v) {
  return v.x * v.x + v.y * v.y + v.z * v.z;
}

#define rad2deg 57.3

};

POINT_CLOUD_REGISTER_POINT_STRUCT(jaguar::PointXYZIT,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (float, intensity, intensity)
                                  (double, timestamp, timestamp))

// Innovusion Jaguar Prime 300 line lidar point format.
struct InnovusionJaguarPointType
{
  PCL_ADD_POINT4D;
  double timestamp;
  unsigned char intensity;
  unsigned char flags;
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
} EIGEN_ALIGN16;

POINT_CLOUD_REGISTER_POINT_STRUCT (InnovusionJaguarPointType,
                                   (float, x, x)
                                   (float, y, y)
                                   (float, z, z)
                                   (double, timestamp, timestamp)
                                   (unsigned char, intensity, intensity)
                                   (unsigned char, flags, flags)
)

typedef jaguar::PointXYZIT TPoint;
typedef pcl::PointCloud<TPoint> TPointCloud;

inline void TPointCloud2VPointCloud(TPointCloud::Ptr input_pc,
                                    jaguar::VPointCloud::Ptr output_pc) {
  output_pc->header = input_pc->header;
  output_pc->height = input_pc->height;
  output_pc->width = input_pc->width;
  output_pc->is_dense = input_pc->is_dense;
  output_pc->resize(output_pc->width * output_pc->height);
  for(size_t h = 0; h < input_pc->height; h++) {
    for(size_t w = 0; w < input_pc->width; w++) {
      jaguar::VPoint point;
      point.x = input_pc->at(w,h).x;
      point.y = input_pc->at(w,h).y;
      point.z = input_pc->at(w,h).z;
      point.intensity = input_pc->at(w,h).intensity;
      output_pc->at(w,h) = point;
    }
  }
}


namespace hesai {
  struct EIGEN_ALIGN16 Point {
      PCL_ADD_POINT4D;
      float intensity;
      double timestamp;
      uint16_t ring;
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  };

  inline void PointCloud2Callback(const sensor_msgs::PointCloud2& cloud_msg, 
    pcl::PointCloud<pcl::PointXYZITR>* cloud) {
    pcl::PointCloud<Point> cloud_tmp;
    pcl::fromROSMsg(cloud_msg, cloud_tmp);
    cloud->points.resize(cloud_tmp.points.size());
    for (size_t i = 0; i < cloud_tmp.points.size(); ++i) {
      cloud->points[i].x = cloud_tmp.points[i].x;
      cloud->points[i].y = cloud_tmp.points[i].y;
      cloud->points[i].z = cloud_tmp.points[i].z;
      cloud->points[i].intensity = cloud_tmp.points[i].intensity;
      cloud->points[i].ring = cloud_tmp.points[i].ring;
      cloud->points[i].timestamp = cloud_tmp.points[i].timestamp;
    }
  }
}  // namespace hesai

POINT_CLOUD_REGISTER_POINT_STRUCT(hesai::Point,
    (float, x, x)
    (float, y, y)
    (float, z, z)
    (float, intensity, intensity)
    (double, timestamp, timestamp)
    (uint16_t, ring, ring)
)

namespace msc_os1_128 {
  struct EIGEN_ALIGN16 PointOS1 {
      PCL_ADD_POINT4D;
      float intensity;
      uint32_t t;
      uint16_t reflectivity;
      uint16_t ring;
      uint16_t ambient;
      uint32_t range;
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  };

  inline void PointCloud2Callback(const sensor_msgs::PointCloud2& cloud_msg, 
    pcl::PointCloud<pcl::PointXYZITR>* cloud) {
    pcl::PointCloud<PointOS1> cloud_tmp;
    pcl::fromROSMsg(cloud_msg, cloud_tmp);
    cloud->points.resize(cloud_tmp.points.size());
    for (size_t i = 0; i < cloud_tmp.points.size(); ++i) {
      cloud->points[i].x = cloud_tmp.points[i].x;
      cloud->points[i].y = cloud_tmp.points[i].y;
      cloud->points[i].z = cloud_tmp.points[i].z;
      cloud->points[i].intensity = cloud_tmp.points[i].intensity;
      cloud->points[i].timestamp = cloud_tmp.points[i].t / 1000000.0; // resulting in millisec timestamps.
      cloud->points[i].ring = cloud_tmp.points[i].ring;
    }
  }
}

POINT_CLOUD_REGISTER_POINT_STRUCT(msc_os1_128::PointOS1,
    (float, x, x)
    (float, y, y)
    (float, z, z)
    (float, intensity, intensity)
    (uint32_t, t, t)
    (uint16_t, reflectivity, reflectivity)
    (uint16_t, ring, ring)
    (uint16_t, ambient, ambient)
    (uint32_t, range, range)
)

namespace coloradar_os1_64 {
  struct EIGEN_ALIGN16 PointOS1 {
      PCL_ADD_POINT4D;
      float intensity;
      uint32_t t;
      uint16_t reflectivity;
      uint8_t ring;
      uint16_t noise;
      uint32_t range;
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  };

  inline void PointCloud2Callback(const sensor_msgs::PointCloud2 &cloud_msg, pcl::PointCloud<pcl::PointXYZITR> *cloud) {
    pcl::PointCloud<PointOS1> cloud_tmp;
    pcl::fromROSMsg(cloud_msg, cloud_tmp);
    cloud->points.resize(cloud_tmp.points.size());
    for (size_t i = 0; i < cloud_tmp.points.size(); ++i) {
      cloud->points[i].x = cloud_tmp.points[i].x;
      cloud->points[i].y = cloud_tmp.points[i].y;
      cloud->points[i].z = cloud_tmp.points[i].z;
      cloud->points[i].intensity = cloud_tmp.points[i].intensity;
      cloud->points[i].timestamp = cloud_tmp.points[i].t / 1000000.0; // resulting in millisec timestamps.
      cloud->points[i].ring = cloud_tmp.points[i].ring;
    }
  }
}

POINT_CLOUD_REGISTER_POINT_STRUCT(coloradar_os1_64::PointOS1,
    (float, x, x)
    (float, y, y)
    (float, z, z)
    (float, intensity, intensity)
    (uint32_t, t, t)
    (uint16_t, reflectivity, reflectivity)
    (uint8_t, ring, ring)
    (uint16_t, noise, noise)
    (uint32_t, range, range)
)

#endif
