
aloam_ws=/media/jhuai/docker/lidarslam/aloam_ws
datadir=/media/jhuai/BackupPlus/jhuai/data/mscrad4r
outdir=/media/jhuai/BackupPlus/jhuai/results/msc_aloam

cd $aloam_ws
source devel/setup.bash

roverbags=(
RURAL_A0
RURAL_A1
RURAL_A2
RURAL_B0
RURAL_B1
RURAL_B2
RURAL_C0
RURAL_C1
RURAL_C2
RURAL_D0
RURAL_D1
RURAL_D2
RURAL_E0
RURAL_E1
RURAL_E2
RURAL_F0
RURAL_F1
RURAL_F2
)

proc_rover() {
    bagname=$1
    bag=$datadir/$bagname/$bagname.bag
    echo "Processing $bag"
    mkdir -p $outdir/$bagname
    roslaunch aloam_velodyne aloam_msc_os1.launch bag:=$bag path_est:=$outdir/$bagname/state_estimates.txt
}

counter=0
for bag in ${roverbags[@]}; do
    proc_rover $bag
    ((counter++))
    # if [ $counter -ge 0 ]; then
    #     break
    # fi
done
