
aloam_ws=/media/jhuai/docker/lidarslam/aloam_ws
datadir=/media/jhuai/BackupPlus/jhuai/data/coloradar/rosbags
outdir=/media/jhuai/BackupPlus/jhuai/results/coloradar_aloam

cd $aloam_ws
source devel/setup.bash

handheldbags=(
aspen_run0.bag
aspen_run1.bag
aspen_run2.bag
aspen_run3.bag
aspen_run4.bag
aspen_run5.bag
aspen_run6.bag
aspen_run7.bag
aspen_run8.bag
aspen_run9.bag
aspen_run10.bag
aspen_run11.bag
arpg_lab_run0.bag
arpg_lab_run1.bag
arpg_lab_run2.bag
arpg_lab_run3.bag
arpg_lab_run4.bag
outdoors_run0.bag
outdoors_run1.bag
outdoors_run2.bag
outdoors_run3.bag
outdoors_run4.bag
outdoors_run5.bag
outdoors_run6.bag
outdoors_run7.bag
outdoors_run8.bag
outdoors_run9.bag
longboard_run0.bag
longboard_run1.bag
longboard_run2.bag
longboard_run3.bag
longboard_run4.bag
longboard_run5.bag
longboard_run6.bag
longboard_run7.bag
edgar_army_run0.bag
edgar_army_run1.bag
edgar_army_run2.bag
edgar_army_run3.bag
edgar_army_run4.bag
edgar_army_run5.bag
ec_hallways_run0.bag
ec_hallways_run1.bag
ec_hallways_run2.bag
ec_hallways_run3.bag
ec_hallways_run4.bag
edgar_classroom_run0.bag
edgar_classroom_run1.bag
edgar_classroom_run2.bag
edgar_classroom_run3.bag
edgar_classroom_run4.bag
edgar_classroom_run5.bag)

proc_handheld() {
    bagname=$1
    bag=$datadir/$bagname
    echo "Processing $bag"
    bagname=${bagname%.*}
    echo "Bagname: $bagname"
    mkdir -p $outdir/$bagname
    roslaunch aloam_velodyne aloam_coloradar_os1.launch bag:=$bag path_est:=$outdir/$bagname/state_estimates.txt
}

counter=0
for bag in ${handheldbags[@]}; do
    proc_handheld $bag
    ((counter++))
    # if [ $counter -ge 0 ]; then
    #     break
    # fi
done

